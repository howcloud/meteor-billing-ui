/**
 * 
 */

var cardBrand2faIcon = {
	'American Express': 'cc-amex',
	'Visa': 'cc-visa',
	'MasterCard': 'cc-mastercard',
	'Discover': 'cc-discover',
	'JCB': 'credit-card',
	'Diners Club': 'credit-card',
	'Unknown': 'credit-card'
}

CardInput = React.createClass({

	/** Component Properties **/

	propTypes: {
		async: React.PropTypes.bool, // a flag for whether we support async value getting for cards (allows us to lazy generate value of input rather than have the user have to click to add card)
	},

	/** State **/

	getInitialState: function () {
		return {
			loading: false,
			showAddCard: false,

			// async: false, // TODO: Switch default to true/remove this option once deprecate legacy use case
		}
	},

	/** Mixins **/

	mixins: [
		React.addons.PureRenderMixin,
		ReactMeteor.Mixin
	],

	/** Statics **/

	statics: {

		/** Meteor **/

		getMeteorState: function (props, state) {
			var _user = Meteor.user();
			var _cards = _user && _user.payment && _user.payment.cards ? _user.payment.cards : [];

			return {
				cards: _cards,
				showAddCard: state.showAddCard || _cards.length === 0 ? true : false,
			}
		},

	},

	/** Value **/
	// Gets the id of the currently selected card or try to generate one if we are in async mode and able to
	// TODO: Make all requests for a value asynchronous, ie deprecate this not being async

	value: function (callback) {
		var selectedCard = _.find(this.state.cards, function (card) {
			return this.refs['cc_'+card.id].getDOMNode().checked ? true : false;
		}.bind(this));

		if (selectedCard) {
			if (callback) callback(null, selectedCard.id);

			return selectedCard.id;
		}

		if (callback) this.generateValue(callback);

		return null;
	},

	// asynchronous processing of a new card to be a value if the user has not got any payment methods added yet

	canGenerateValue: function () {
		var cardNumber = this.refs.cardNumber.value();
		var cvc = this.refs.cvc.value();
		var expMonth = this.refs.expMonth.value();
		var expYear = this.refs.expYear.value();

		if (!cardNumber || !cvc || !expMonth || !expYear) return false;
	},
	generateValue: function (callback, alertErrors) {
		var cardNumber = this.refs.cardNumber.value();
		var cvc = this.refs.cvc.value();
		var expMonth = this.refs.expMonth.value();
		var expYear = this.refs.expYear.value();

		if (!cardNumber || !cvc || !expMonth || !expYear) {
			if (alertErrors) alert('Please fill in all of your card details before proceeding');
			if (callback) callback(true, null);

			return;
		}

		this.setState({loading: true});

		deferUntilStripeLoaded(function () {

			Stripe.card.createToken({
				number: cardNumber,
				cvc: cvc, 
				exp_month: expMonth,
				exp_year: expYear
			}, function (status, response) {
				if (!this.isMounted()) return;

				if (response.error) {
					if (alertErrors) alert('There was an error verifying your card details - please try again');
					if (callback) callback(true, null);

					this.setState({loading: false});
					
					return;
				}

				var token = response.id;

				Meteor.user().addStripeCard(token, function (err, result) {
					this.setState({loading: false});

					if (err || !result) {
						if (alertErrors) alert('There was an error verifying your card details - please try again');
						if (callback) callback(true, null);
						
						return;
					}

					if (callback) callback(null, result); // result = the id of the card added

					this.setState({showAddCard: false});
				}.bind(this));
			}.bind(this));

		}.bind(this));
	},

	/** React **/

	componentDidMount: function () {
		if (this.refs && this.refs.cardNumber) this.refs.cardNumber.focus();
	},

	/** Render **/

	render: function () {
		var _disabled = this.state.loading || this.props.disabled ? true : false;

		var cards = _.map(this.state.cards, function (card, index) {
			var icon = cardBrand2faIcon[card.brand];
			if (!icon) icon = 'credit-card';

			var id = "cc_"+card.id;

			return (
				<div key={index} className="card">
					<input type="radio" ref={id} id={id} name="cc_choose" value={card.id} defaultChecked={card.default ? true : false} disabled={_disabled} /> 
					<FontAwesome icon={icon} /> 
					<label htmlFor={id}><span className="strong">{card.last4}</span> <span className="sub">Expires {card.expMonth}/{card.expYear}</span></label>
				</div>
			);
		});

		return (
			<div className={["cardInput", this.props.className || ""].join(' ')} style={this.props.style}>
				{cards}

				{cards.length && !this.state.showAddCard ? <p className="optlinks">
					<a onClick={this.showAddCard_click}><FontAwesome icon="plus" />add card</a>
				</p> : null}

				{this.state.showAddCard ? <table className="formtable">
					<tr>
						<td className="labelCell">
							<label>Card Number</label>
						</td>
						<td className="inputCell">
							<TextInput ref="cardNumber" onEnterUp={this.addCard_submit} disabled={_disabled} />
						</td>
					</tr>
					<tr>
						<td className="labelCell">
							<label>CVC</label>
						</td>
						<td className="inputCell">
							<TextInput ref="cvc" width={70} onEnterUp={this.addCard_submit} disabled={_disabled} />
						</td>
					</tr>
					<tr>
						<td className="labelCell">
							<label>Expiry</label>
						</td>
						<td className="inputCell">
							<TextInput ref="expMonth" placeholder="MM" width={35} onEnterUp={this.addCard_submit} disabled={_disabled} /> <TextInput ref="expYear" placeholder="YY" width={35} onEnterUp={this.addCard_submit} disabled={_disabled} />
						</td>
					</tr>
					{cards.length || !this.props.async ? <tr>
						<td>
							
						</td>
						<td className="inputCell">
							{this.state.loading ?
							<div className="minHeightButton">
								<img src="/img/UI/load-gray.gif" />
							</div>
							:
							<Button onClick={this.addCard_submit} inline={true} disabled={_disabled}><FontAwesome icon="plus" /> Add Payment Method</Button>
							}
						</td>
					</tr> : null}
				</table> : null}
			</div>
		);
	},

	/** UI Events **/

	showAddCard_click: function () {
		this.setState({showAddCard: true});
	},

	addCard_submit: function () {
		this.generateValue(null, true);
	},

});
