/**
 * 
 */

CouponInput = React.createClass({

	/** Component Properties **/

	propTypes: {
		defaultCouponIdentifier: React.PropTypes.string,
		defaultShowInput: React.PropTypes.bool,
		inlin: React.PropTypes.bool, // whether to render in inline mode (ie status text next to input)
		onChange: React.PropTypes.func, // a callback when the coupon that the user wants to apply has changed
	},
	getDefaultProps: function () {
		return {
			defaultShowInput: false
		}
	},

	/** State **/

	getInitialState: function () {
		return {
			couponIdentifier: this.props.defaultCouponIdentifier,
			showInput: (this.props.defaultShowInput || this.props.defaultCouponIdentifier) ? true : false
		}
	},

	/** Mixins **/

	mixins: [
		React.addons.PureRenderMixin,
		ReactMeteor.Mixin
	],

	/** Statics **/

	statics: {

		/** Meteor **/

		meteorStateDeps: ['couponIdentifier'],

		meteorSubscribe: function (props, state) {
			var ret = state.couponIdentifier ? [Meteor.subscribe("Coupon_byIdentifier", state.couponIdentifier)] : [];

			window.subbed = ret;

			return ret;
		},
		getMeteorState: function (props, state) {
			return {
				coupon: state.couponIdentifier ? Coupons.getByIdentifier(state.couponIdentifier) : null
			}
		},

	},

	/** Value **/

	value: function () {
		var _coupon = this.state.coupon;
		var _couponIdentifier = this.state.couponIdentifier;
		
		if (_coupon && !_coupon.active) {
			_coupon = null;
			_couponIdentifier = null;
		}

		return {
			coupon: _coupon,
			couponIdentifier: _couponIdentifier
		}
	},

	// setCouponIdentifier:
	// set a coupon identifier which this CouponInput should be using
	// this is a bit like setValue but without requiring the complete value structure we get from this.value calls

	setCouponIdentifier: function (couponIdentifier) {
		this.setState({
			showInput: true
		}, function () {
			this.refs.cinput.setValue(couponIdentifier);
			this.add_click();
		}.bind(this));
	},

	/** Focus **/

	focus: function () {
		if (this.state.showInput) {
			this.refs.cinput.focus();
		} else {
			this.showInput_click(); // shows it and then focuses on it
		}
	},

	/** React **/

	componentWillUpdate: function(nextProps, nextState) {
		
	},
	componentDidMount: function () {
		if (this.refs && this.refs.cardNumber) this.refs.cardNumber.focus();
	},
	componentDidUpdate: function(prevProps, prevState) {
		if (!_.isEqual(prevState.coupon, this.state.coupon)) {
			if (this.props.onChange) this.props.onChange();
		}	
	},

	/** Render **/

	render: function () {
		if (this.state.showInput) {
			var message;

			if (this.state.couponIdentifier) {
				if (this.state._meteorLoading) {
					message = (
						<span className="sub">
							<img src="/img/load-gray.gif" style={{marginRight: 5}} /> loading coupon...
						</span>
					);
				} else {
					if (!this.state.coupon) {
						message = (
							<span className="sub error">
								This coupon code is invalid :(
							</span>
						);
					} else if (!this.state.coupon.active) {
						message = (
							<span className="sub error">
								This coupon code is no longer active :(
							</span>
						);
					} else {
						message = (
							<span className="sub success">
								<FontAwesome icon="check" /> Coupon applied
							</span>
						);
					}
				}
			}

			return (
				<div className={this.props.className} style={this.props.style}>
					<div className="inline-children">
						<TextInput ref="cinput" onChange={this.cinput_change} size={this.props.size} width={200} placeholder="Enter coupon code" defaultValue={this.props.defaultCouponIdentifier} onEnterUp={this.add_click} />
						<Button size={this.props.size} onClick={this.add_click} style={{marginRight: this.props.inline ? 10 : null}}><FontAwesome icon="plus" /></Button>
						
						{this.props.inline ? message : null}
					</div>
					
					{message && !this.props.inline ? <div style={{marginTop: 10}}>
						{message}
					</div> : null}
				</div>
			);
		} else {
			return (
				<div className={this.props.className} style={this.props.style}>
					<a className="optlink" onClick={this.showInput_click}><FontAwesome icon="gift" />Enter Coupon</a>
				</div>
			);
		}
	},

	/** UI Events **/

	// we don't want to update the couponIdentifier unless we explicitly hit enter or add the newly entered coupon code
	// cinput_change: function () {
	// 	var _value = this.refs.cinput.value();

	// 	this.setState({
	// 		inputValue: _value,
	// 		couponIdentifier: _value == this.state.inputValue ? this.state.couponIdentifier : null
	// 	});
	// },

	add_click: function () {
		this.setState({
			couponIdentifier: this.refs.cinput.value()
		});
	},
	showInput_click: function () {
		this.setState({
			showInput: true
		}, function () {
			this.refs.cinput.focus();
		}.bind(this));
	},

});
