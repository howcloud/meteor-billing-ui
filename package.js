Package.describe({
	name: 'howcloud:billing-ui',
	version: '0.1.0',
	summary: 'Exports UI components to capture payments',
});

Package.on_use(function (api) {

	api.use('howcloud:react');
	api.use('howcloud:react-deps');
	api.use('howcloud:react-build');
	api.use('howcloud:ui-base');

	api.use('howcloud:billing');

	api.add_files('components/CardInput.jsx', ['client', 'server']);
	api.add_files('components/CouponInput.jsx', ['client', 'server']);

	api.export('CardInput', ['client', 'server']);
	api.export('CouponInput', ['client', 'server']);

});